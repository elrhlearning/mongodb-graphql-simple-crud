const mongo = require('mongoose');
const Schema = mongo.Schema;

const userSchema = new Schema({
    firstName: String,
    lastName: String,
    country: String,
});

module.exports = mongo.model('User', userSchema);
