const UserSchema = require('./../types/user-type');
const UserModel = require('./../../models/user');
const graphql = require('graphql');
const {
    GraphQLID,
} = graphql;

const findById = {
    type: UserSchema,
    args: {
        id: {
            type: GraphQLID
        }
    },
    resolve(parent, args) {
        return UserModel.findById(args.id)
    }
}

module.exports = findById;
