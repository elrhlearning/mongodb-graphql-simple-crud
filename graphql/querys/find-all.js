const UserSchema = require('./../types/user-type');
const UserModel = require('./../../models/user');
const graphql = require('graphql');
const {
    GraphQLList,
} = graphql;


const findAll = {
    type: new GraphQLList(UserSchema),
    resolve(parent, args) {
        return UserModel.find({});
    }
}

module.exports = findAll;
