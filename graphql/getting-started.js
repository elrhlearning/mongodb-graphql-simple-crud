const graphql = require('graphql');
const findById = require('./querys/find-by-id')
const findAll = require('./querys/find-all');
const addUser = require('./mutations/add-user');
const deleteUser = require('./mutations/delete-user');
const {
    GraphQLObjectType,
    GraphQLSchema,
} = graphql;

const RootQuery = new GraphQLObjectType({
    name: 'GettingStartedGraphQL',
    fields: {
        user: findById,
        users: findAll
    }
});
const Mutation = new GraphQLObjectType({
    name: 'UserMutations',
    fields: {
        addUser,
        deleteUser
    }
})

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation,
});
