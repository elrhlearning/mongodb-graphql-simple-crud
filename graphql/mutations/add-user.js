const UserSchema = require('./../types/user-type');
const UserModel = require('./../../models/user');
const graphql = require('graphql');
const {
    GraphQLString,
    GraphQLNonNull
} = graphql;

const addUser = {
    type: UserSchema,
        args: {
        firstName: {
            type: new GraphQLNonNull(GraphQLString)
        },
        lastName: {
            type: new GraphQLNonNull(GraphQLString)
        },
        country: {
            type: new GraphQLNonNull(GraphQLString)
        }
    },
    resolve(parent, args) {
        let user = new UserModel({
            firstName: args.firstName,
            lastName: args.lastName,
            country: args.country,
        });
        return user.save();
    }
};

module.exports = addUser;
