const UserSchema = require('./../types/user-type');
const UserModel = require('./../../models/user');
const graphql = require('graphql');
const {
    GraphQLID,
} = graphql;

const deleteUser = {
    type: UserSchema,
        args: {
        id: {
            type: GraphQLID
        }
    },
    async resolve(parent, args) {
        let user = await UserModel.findById(args.id);
        console.log(user)
        const result = await user.deleteOne();
        console.log(result)
        return user;
    }
}

module.exports = deleteUser;
