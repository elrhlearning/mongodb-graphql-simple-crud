var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var { graphqlHTTP } = require('express-graphql');
const mongo = require('mongoose');
var app = express();

mongo.connect('mongodb://localhost:27017/graphql', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

mongo.connection.once('open', () => {
  console.log('connected to database');
})

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const GettingStartedApiConfig = require('./graphql/getting-started');
app.use('/graphql-getting-started-api', graphqlHTTP({ schema: GettingStartedApiConfig, graphiql: true}));

module.exports = app;
